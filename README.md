# MSA_pipeline

The msa_pipeline bundles existing tools to make multiple alignment of genomes easy. A simplified schema of the pipeline is shown below.

![Scheme](workflow/images/msa_pipeline_scheme.jpg){width=20%}

## Quickstart

Set up the pipeline.

```
git clone https://bitbucket.org/bucklerlab/msa_pipeline.git
```

The pipeline is parametrised using a single config file that can be generated based on the provided template `./config/config.yaml`. The only required file input is a set of fasta files with the suffix `.fa` that should be placed in the directory `msa_pipeline/data`. The input files must be referenced in the config file. The pipeline can then be executed in two steps as follows.

```
snakemake --use-conda -j 5 --configfile config/myconfig.yaml -R align
snakemake --use-conda -j 1 --configfile config/myconfig.yaml -R roast
```

The practical reason for splitting the pipeline into two steps is that the first pairwise alignment step `align` is thread-intensive and the second multiple alignment step `roast` is single-threaded but memory-intensive and time-intensive. The two steps thus allow adjustment of computational resources, e.g. when submitting the pipeline as single scheduled jobs.

A final optional step will generate GERP and phyloP conservation scores based on the previously generated multiple alignment.

```
snakemake --use-conda -j 5 --configfile config/myconfig.yaml -R call_conservation
```

## Testing the pipeline

To test the pipeline before running on your own data, you can align some simulated mammalian sequences. This run should complete in <5 min on a desktop computer and uses 1 thread.

```
cd msa_pipeline
bash .test/mammals.sh
```

All output files are written to `.test/mammals_results_{RANDOM_STR}` and the main multiple alignment output file is written to `.test/mammals_results_{RANDOM_STR}/roast/roast.maf`.

A larger set of yeast and plant genomes can also be used to test the pipeline and the parallelization efficiency. By default these test runs use 4 threads and should run in <1h on a standard laptop.

```
bash .test/yeast.sh
bash .test/arabidopsis.sh
```

The config files for each of the three test runs are written to show different ways of parametrising the pipeline. 

```
.test/mammals_config.yaml # Use last without splitting input fasta files
.test/yeast_config.yaml # Split input fasta files for improved parallelization of last
.test/arabidopsis_config.yaml # Use minimap2 aligner
```

### Modifying pipeline parameters

Alignment parameters can be modified in the config file. For example, to run an alignment using the HOXD70 matrix with custom penalty setting the line could be changed to:

```
lastParams: "-j 3 -u 1 -m50 -p HOXD70 -a 700 -b 20 -e 5000 -d 3500"
```

For more details on LAST parameters, see the official [LAST documentation](https://gitlab.com/mcfrith/last).


### Running the pipeline using a container

In some cases it may be useful to execute the pipeline using a singularity container rather than locally installing the conda environments. To do this, users only need to add the `--use-singularity` flag to their snakemake command. For example:

```
snakemake --use-conda -p -j 4 --use-singularity --configfile config/myconfig.yaml -R align
```

To test whether singularity is working correctly, the test analyses described above can all also be executed using singularity. To do this, execute the bash wrapper as above with the `--use-singularity` flag as an argument.

```
bash .test/mammals.sh --use-singularity 
```

### Software requirements
   
Snakemake is required to run this pipeline. The recommended installation is shown below. For more details see [snakemake installation guide](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html).

```
conda install -n base -c conda-forge mamba
conda activate base
mamba create -c conda-forge -c bioconda -n snakemake snakemake
```

Note that the installation should use the exact commands above, including the exact channel priority, otherwise snakemake may be improperly installed.

To run the pipeline using a prebuilt singularity container, you must have singularity installed on your system. Singularity is installed on many HPC systems and can be used without root privileges. However, note that installation of singularity does require root privileges. If you want to install singularity and have these privileges, you can find up-to-date instructions on how to do so in the official [singularity installation guide](https://github.com/sylabs/singularity/blob/master/INSTALL.md).

### Explanation of output files

Results of each `msa_pipeline` run are written to the `/results` directory, which contains several subdirectories with outputs from different steps of the pipeline. The file tree below shows typical outputs of the pipeline. The key output files for most users are likely the final multiple alignment `roast/roast.maf` and the directories `phylop` and `gerp`.

```bash
.
`-- results
    |-- conservation_raw # raw phylop/gerp outputs
    |-- genome # fasta indexes
    |-- gerp # GERP conservation scores
    |   |-- chr1.gerp.final.bed
    |   |-- chr2.gerp.final.bed
    |   |-- chr3.gerp.final.bed
    |   |-- chr4.gerp.final.bed
    |   `-- chr5.gerp.final.bed
    |-- phylop # phyloP conservation scores
    |   |-- chr1.phylop.final.bed
    |   |-- chr2.phylop.final.bed
    |   |-- chr3.phylop.final.bed
    |   |-- chr4.phylop.final.bed
    |   `-- chr5.phylop.final.bed
    |-- psl # query alignments in PSL format
    |   |-- Ahalleri.psl
    |   |-- Alyrata.psl
    |   `-- Alyrata_pret.psl
    |-- roast # multiple alignment output
    |   `-- roast.maf
    |-- toast # pairwise MAF alignments
    |   |-- TAIR10.Ahalleri.toast2.maf
    |   |-- TAIR10.Alyrata.toast2.maf
    |   `-- TAIR10.Alyrata_pret.toast2.maf
    `-- tree
        |-- neutral.mod # neutral model inferred by phylofit
        |-- neutral.tre # tree with branch lengths based on neutral model
        |-- raxml # RAxML output
        |-- sitefold.bed # cds_fold.py output of 4-fold degenerate sites
        `-- topology.nwk # taxa topology based on user input or mashtree
```


## Troubleshooting

### Common errors

#### Input file errors
When the snakemake run terminates with an error despite snakemake being correctly installed, there are several common causes related to input files:

- Input FASTA files in the /data directory do not match samples listed in the config file parameters `species` and `refName`
- Input FASTA files have chromosomes/scaffolds with special characters; ideally, we want names consisting of only alphanumeric characters and "_"
- When providing a GFF file to calculate a neutral model from fourfold degenerate sites, the GFF chromosome/scaffold names do not match the reference FASTA
- Check the optional custom parameters in the config file and ensure that they are valid for the respective aligner

You can check the log files specific to each rule for other potential sources of error. 

#### Conda-related errors

In some HPC environments I have also come across errors when a $PYTHONPATH variable is set, which can lead to wires getting crossed and incompatibilities between python version. Conda does not use $PYTHONPATH so these issues can be resolved by unsetting it.
```
unset PYTHONPATH
```
For further issues related to conda environments, a solution may be to run the pipeline using the singularity container (see section above 'Running the pipeline using a container'). 

### Patching netToAxt for large, fragmented alignments

For diverged large plant genomes, it is common that large numbers of alignment chains are produced, which can lead to to `netToAxt` error: `chainId 273107809, can only handle up to 268435456`. Currently the only way to address this issue is to patch and recompile the utility and then to hardcode the path into the relevant snakemake rule net_to_axt_to_maf which can be found in `workflow/rules/chain_and_net.smk`. The script below compiles a patched netToAxt in `$(pwd)/bin`. 

```
wget http://hgdownload.cse.ucsc.edu/admin/exe/userApps.archive/userApps.v421.src.tgz
tar -xvzf userApps.v421.src.tgz
export MACHTYPE=x86_64
export BINDIR=$(pwd)/bin
export L="${LDFLAGS}"
mkdir -p "$BINDIR"
(cd userApps/kent/src/lib && make)
(cd userApps/kent/src/htslib && make)
(cd userApps/kent/src/jkOwnLib && make)
(cd userApps/kent/src/hg/lib && make)
(cd userApps/kent/src/hg/mouseStuff/netToAxt &&  sed -i 's/maxChainId (256\*1024\*1024)/maxChainId (256\*1024\*1024\*6)/' netToAxt.c && make)
chmod +x $BINDIR/netToAxt
```

### How to cite

If you use msa_pipeline in your work, please cite:

> Yaoyao Wu, Lynn Johnson, Baoxing Song, Cinta Romay, Michelle Stitzer, Adam Siepel, Edward Buckler, Armin Scheben (2021)
> A multiple genome alignment workflow shows the impact of repeat masking and parameter tuning on alignment of functional regions in plants  
> *bioRxiv* 2021.06.01.446647. [doi:10.1101/2021.06.01.446647](https://doi.org/10.1101/2021.06.01.446647)

