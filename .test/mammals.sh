#!/usr/bin/env bash
PARAM=""
SNAKEFILE="workflow/Snakefile"
OUTDIR="data"
# Can be set to "--use-singularity"

if [ -z "$1" ]; then
    SINGULARITY=""
elif [ $1 = "--use-singularity" ]; then
    SINGULARITY="--use-singularity"   
else
    echo "Unrecognized positional argument. Only '--use-singularity' is accepted."
fi

if ! command -v snakemake &> /dev/null; then
    echo "snakemake not available. Exiting ..."
    exit 1
fi

USECONDA=""
# check if mamba is available
if ! command -v mamba &> /dev/null; then
    echo "The mamba package could not be found. Will use conda."
    USECONDA="--conda-frontend conda"
fi

if [ -f "$SNAKEFILE" ]; then
    echo "Found Snakefile: $SNAKEFILE. Executing test analysis."
else 
    echo "Snakefile not found. Expect in: $SNAKEFILE. This script must be executed from the directory /msa_pipeline"
    exit 1
fi

WGET="wget --no-check-certificate"
CURL="curl"

sleep 3s

cow="https://raw.githubusercontent.com/UCSantaCruzComputationalGenomicsLab/cactusTestData/master/evolver/mammals/loci1/simCow.chr6"
dog="https://raw.githubusercontent.com/UCSantaCruzComputationalGenomicsLab/cactusTestData/master/evolver/mammals/loci1/simCow.chr6"
human="https://raw.githubusercontent.com/UCSantaCruzComputationalGenomicsLab/cactusTestData/master/evolver/mammals/loci1/simHuman.chr6"
mouse="https://raw.githubusercontent.com/UCSantaCruzComputationalGenomicsLab/cactusTestData/master/evolver/mammals/loci1/simMouse.chr6"
rat="https://raw.githubusercontent.com/UCSantaCruzComputationalGenomicsLab/cactusTestData/master/evolver/mammals/loci1/simRat.chr6"

## Download data
$WGET $cow -O $OUTDIR/simCow.fa 2>/dev/null || $CURL $cow -o $OUTDIR/simCow.fa
sed '/^>/s/.*chr/>chr/' $OUTDIR/simCow.fa > $OUTDIR/simCow.fa.tmp && mv $OUTDIR/simCow.fa.tmp $OUTDIR/simCow.fa
$WGET $dog -O $OUTDIR/simDog.fa 2>/dev/null || $CURL $dog -o $OUTDIR/simDog.fa
sed '/^>/s/.*chr/>chr/' $OUTDIR/simDog.fa > $OUTDIR/simDog.fa.tmp && mv $OUTDIR/simDog.fa.tmp $OUTDIR/simDog.fa
$WGET $human -O $OUTDIR/simHuman.fa 2>/dev/null || $CURL $human -o $OUTDIR/simHuman.fa
sed '/^>/s/.*chr/>chr/' $OUTDIR/simHuman.fa > $OUTDIR/simHuman.fa.tmp && mv $OUTDIR/simHuman.fa.tmp $OUTDIR/simHuman.fa
$WGET $mouse -O $OUTDIR/simMouse.fa 2>/dev/null || $CURL $mouse -o $OUTDIR/simMouse.fa
sed '/^>/s/.*chr/>chr/' $OUTDIR/simMouse.fa > $OUTDIR/simMouse.fa.tmp && mv $OUTDIR/simMouse.fa.tmp $OUTDIR/simMouse.fa
$WGET $rat -O $OUTDIR/simRat.fa 2>/dev/null || $CURL $rat -o $OUTDIR/simRat.fa
sed '/^>/s/.*chr/>chr/' $OUTDIR/simRat.fa > $OUTDIR/simRat.fa.tmp && mv $OUTDIR/simRat.fa.tmp $OUTDIR/simRat.fa

if [[ -s "$OUTDIR/simCow.fa" ]] && [[ -s "$OUTDIR/simDog.fa" ]] && [[ -s "$OUTDIR/simHuman.fa" ]] && [[ -s "$OUTDIR/simMouse.fa" ]]  && [[ -s "$OUTDIR/simRat.fa" ]]; then
    echo "Downloaded all test data."
else
    echo "Download failed! Check whether wget or curl are available and that hyperlinks are live"
    exit 1
fi


## Run pipeline in three steps
snakemake --use-conda --rerun-incomplete -j 1 $SINGULARITY $USECONDA --configfile .test/mammals_config.yaml -R align
snakemake --use-conda --rerun-incomplete -p -j 1 $SINGULARITY $USECONDA --configfile .test/mammals_config.yaml -R roast
snakemake --use-conda --rerun-incomplete -j 1 $SINGULARITY $USECONDA --configfile .test/mammals_config.yaml -R call_conservation

## Move results directory
random=$(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom | head -c 13)
mv results .test/mammals_results_$random
echo "Completed tests analysis, if there are no errors, check for results in .test/mammals_results_$random"
